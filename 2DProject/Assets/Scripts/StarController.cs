﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StarController : MonoBehaviour
{
    public TextMeshProUGUI starCount;
    public GameObject winTextObject;

    private int count;

    void Start()
    {
        winTextObject.SetActive(false);
    }

    public void IncreaseCount()
    {
        count = count + 1;
        SetStarCount();
    }

    void SetStarCount()
    {
        starCount.text = "Stars: " + count.ToString();

        if (count >= 5)
        {
            winTextObject.SetActive(true);
        }
    }
}
