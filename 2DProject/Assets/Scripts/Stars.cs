﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Stars : MonoBehaviour
{
    public TextMeshProUGUI starCount;
    public StarController sp;
    public GameObject star;

    private int count;


    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Collision with " + other.gameObject.CompareTag("Player"));
        
        if (other.gameObject.CompareTag("Player"))
        {
            sp.IncreaseCount();

            Destroy(gameObject);
        }
    }

}
